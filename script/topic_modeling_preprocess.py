import pandas as pd
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import cld2

stop_words = stopwords.words('english')
stemmer = PorterStemmer()

def preprocess(input, twitter=False):
    series = pd.read_pickle(input)
    df = pd.DataFrame(series)
    df['tract'] = df.index
    df.columns = ['text', 'tract']
    for i in range(len(df)):
        print("...", i, "/", len(df), "...")
        tract = df['tract'].iloc[i]
        review_tract = df['text'].iloc[i]
        if not twitter:
            review_tract = [item for sublist in review_tract for item in sublist]
        review_tract = filter_english(review_tract)
        preprocessed = []
        for sentence in review_tract:
            text = re.sub("((\S+)?(http(s)?)(\S+))|((\S+)?(www)(\S+))|((\S+)?(\@)(\S+)?)",
                          " ", sentence)
            text = re.sub("[^a-zA-Z ]", "", text)
            text = text.lower()
            text = nltk.word_tokenize(text)
            text = [word for word in text if word not in stop_words]
            text = [stemmer.stem(word) for word in text]
            preprocessed.append(text)
        preprocessed = [item for sublist in preprocessed for item in sublist]
        df.loc[tract]['text']=preprocessed
    return df

def filter_english(lst):
    result = []
    for sentence in lst:
        try:
            _,_,details = cld2.detect(sentence)
            if details[0][0] == 'ENGLISH':
                result.append(sentence)
        except (TypeError, ValueError) as e:
            pass
    return result

def apply_and_output(input, output, twitter=False):
    df = preprocess(input, twitter)
    df.to_pickle(output)

apply_and_output(input = "../data/tripadvisor_att/att_by_tract.pickle",
                 output = "../data/tripadvisor_att/att_by_tract_cleaned.pkl")
apply_and_output(input = "../data/tripadvisor_res/res_by_tract.pickle",
                 output = "../data/tripadvisor_res/res_by_tract_cleaned.pkl")
apply_and_output(input = "../data/twitter/tweets_by_tract.pkl",
                 output = "../data/twitter/tweets_by_tract_cleaned.pkl",
                 twitter=True)


