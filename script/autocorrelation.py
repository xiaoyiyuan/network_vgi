import geopandas as gpd
import pandas as pd
import json
import libpysal
import esda


all_shp = gpd.read_file("../data/singleton/Tracts/Tracts.shp")

def get_Moran(c, cid):
    '''
    test Moran's I for one cid
    select communities from the cid
    assign 1 for this community, 0 for rest
    '''
    tracts = [item for lst in c for item in lst]
    shp = all_shp[all_shp['GEOID10'].isin(tracts)][['GEOID10', 'geometry']]
    community = c[cid]
    shp['value'] = 0
    shp.loc[shp['GEOID10'].isin(community), 'value'] = 1
    assert (len(shp[shp['value'] ==1]) == len(community))

    weights = libpysal.weights.Queen.from_dataframe(shp)
    autocorr = esda.Moran(shp['value'], weights)
    return autocorr.I, autocorr.p_sim


def get_Moran_all(c, cids):
    '''
    get Moran's I and p value for all data, return a dataframe of the results
    '''
    df = pd.DataFrame(columns = ['community_id', 'moran_i', 'moran_p'])
    for cid in cids:
        i, p = get_Moran(c, cid)
        df = df.append({"community_id": int(cid),
                       "moran_i": i,
                       "moran_p": p}, ignore_index=True)
    return df


with open('../networks/all_communities.json') as json_file:
    communities = json.load(json_file)
att_c = communities['att_communities']
res_c = communities['res_communities']
twt_c = communities['twt_communities']
# cids are the biggest n communities (to be consistent with publication viz)
att_cids= [0, 3, 5, 7, 11, 12]
res_cids = [1, 2, 7, 9, 12, 13, 16, 22, 43]
twt_cids = [9, 11, 16, 17, 23]

att_df = get_Moran_all(att_c, att_cids)
res_df = get_Moran_all(res_c, res_cids)
twt_df = get_Moran_all(twt_c, twt_cids)